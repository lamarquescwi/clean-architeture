import ParkingLot from "../entity/ParkingLot";
import ParkingLotRepository from "../repository/ParkingLotRepository";
import ParkedCar from "../entity/ParkedCar";

export default class EnteredParkingLot {

    private ParkingLotRepository: ParkingLotRepository;

    constructor(ParkingLotRepository: ParkingLotRepository) {
        this.ParkingLotRepository = ParkingLotRepository;
    }

    async execute(code: string, plate: string, date: Date) {
        const parkingLot = await this.ParkingLotRepository.getParkingLot(code);
        const parkedCar = new ParkedCar(code, plate, date);
        if(!parkingLot.isOpen(parkedCar.date)) {
            throw new Error("The parking lot is closed.");
        }
        if (parkingLot.isFull()) {
            throw new Error("The parking lot is full.");
        }
        await this.ParkingLotRepository.saveParkedCar(parkedCar.code, parkedCar.plate, parkedCar.date);
        return parkingLot;
    }
}