import ParkingLotRepository from "../../core/repository/ParkingLotRepository";
import ParkingLot from "../../core/entity/ParkingLot";
import database from "../database/database";
import ParkingLotAdapter from "../../adapter/ParkingLotAdapter";

export default class ParkingLotRepositorySQL implements ParkingLotRepository{

    async getParkingLot(code: string): Promise<ParkingLot> {
        const parkingLotData = await database.oneOrNone("select *, (select count(*) from parked_car pc WHERE pc.code = pl.code) as occupied_spaces from parking_lot pl where pl.code = $1", [code]);
        return Promise.resolve(ParkingLotAdapter.create(parkingLotData.code, parkingLotData.capacity, parkingLotData.open_hour, parkingLotData.close_hour, parkingLotData.occupied_spaces));
    }

    async saveParkedCar(code: string, plate: string, date: Date): Promise<void> {
        await database.none("insert into parked_car (code, plate, date) values ($1, $2, $3)", [code, plate, date]);
    }

}